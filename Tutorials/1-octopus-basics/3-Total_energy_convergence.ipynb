{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Total energy convergence\n",
    "[Link to tutorial](https://octopus-code.org/documentation/14/tutorial/basics/total_energy_convergence/)\n",
    "\n",
    "In this tutorial we will show how to converge a quantity of interest (the total energy) with respect to the parameters that define the real-space grid. For this we will use two examples, the Nitrogen atom from the [Basic input options](2-Basic_input_options.ipynb) tutorial, and a methane molecule.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pandas as pd\n",
    "from postopus import Run\n",
    "from string import Template\n",
    "import subprocess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 3-total_energy_convergence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 3-total_energy_convergence"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4",
   "metadata": {},
   "source": [
    "## Nitrogen atom: finding a good spacing\n",
    "\n",
    "The key parameter of a real-space calculation is the spacing between the points of the mesh. The default option in **Octopus** is to use a Cartesian grid. This is a regular grid where the spacing along each Cartesian direction is the same. The first step in any calculation should then be making sure that this spacing is good enough for our purposes. This should be done through a convergence study, very similar to the ones performed in plane-wave calculations.\n",
    "\n",
    "The needed spacing essentially depends on the pseudopotentials that are being used. The idea is to repeat a series of ground-state calculations, with identical input files except for the grid spacing. There are many different ways of doing it, the simplest one being to change the input file by hand and run **Octopus** each time. Another option is to use a little python script like the one bellow.\n",
    "\n",
    "As a first example, we will use the Nitrogen atom from the previous tutorial.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5",
   "metadata": {},
   "outputs": [],
   "source": [
    "inp_template_Nitrogen = Template(\n",
    "    \"\"\"\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "stdout = 'stdout_td_$Spacing_value.txt'\n",
    "stderr = 'stderr_td_$Spacing_value.txt'\n",
    "\n",
    "Nitrogen_mass = 14.0\n",
    "\n",
    "%Species\n",
    "'N' | species_pseudo | set | standard | lmax | 1 | lloc | 0 | mass | Nitrogen_mass\n",
    "%\n",
    "\n",
    "XYZCoordinates = 'N.xyz'\n",
    "\n",
    "ExtraStates = 1\n",
    "%Occupations\n",
    "2 | 1 | 1 | 1\n",
    "%\n",
    "\n",
    "BoxShape = sphere\n",
    "Radius = 5.0*angstrom\n",
    "Spacing = $Spacing_value*angstrom\n",
    "\"\"\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6",
   "metadata": {},
   "outputs": [],
   "source": [
    "N_xyz_string = \"\"\"\n",
    "\n",
    "\n",
    "1\n",
    "This is a comment line\n",
    "N 0 0 0\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_scf_info(run_path: str):\n",
    "    \"\"\"\n",
    "    Extract Total energy , seigen, peigen from info.\n",
    "    \"\"\"\n",
    "    info = Run(run_path).default.scf.info\n",
    "\n",
    "    total_energy = [lines for lines in info if \"Total       = \" in lines][-1]\n",
    "    total_energy = float(total_energy.split(\"=\")[-1])\n",
    "\n",
    "    s_eigen = [lines for lines in info if \"1   --\" in lines][-1]\n",
    "    s_eigen = float(s_eigen.split()[2])\n",
    "\n",
    "    p_eigen = [lines for lines in info if \"2   --\" in lines][-1]\n",
    "    p_eigen = float(p_eigen.split()[2])\n",
    "    return total_energy, s_eigen, p_eigen"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "### Create runs for different spacings\n",
    "What this script does is quite simple: it runs **Octopus** for a list of spacings (0.26, 0.24, ..., 0.14) and after each calculation it collects the values of the total energy and of the eigenvalues. In order to change the value of the spacing between each calculation, it uses the [python string template](https://docs.python.org/3/library/string.html#string.Template). Also for each spacing a separate directory is used.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "list_of_spacing = [0.28, 0.26, 0.24, 0.22, 0.20, 0.18, 0.16, 0.14]\n",
    "\n",
    "for spacing in list_of_spacing:\n",
    "    print(f\"Running octopus for spacing: {spacing}\")\n",
    "    os.makedirs(f\"Nitrogen/run_{spacing}\")\n",
    "    with open(f\"Nitrogen/run_{spacing}/inp\", \"w\") as inp_file:\n",
    "        inp_file.write(inp_template_Nitrogen.substitute(Spacing_value=spacing))\n",
    "    with open(f\"Nitrogen/run_{spacing}/N.xyz\", \"w\") as N_xyz_file:\n",
    "        N_xyz_file.write(N_xyz_string)\n",
    "    subprocess.run(\"octopus\", shell=True, cwd=f\"Nitrogen/run_{spacing}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "### Retrieve and combine data for different spacings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "11",
   "metadata": {},
   "outputs": [],
   "source": [
    "table = []\n",
    "for spacing in list_of_spacing:\n",
    "    total_energy, s_eigen, p_eigen = get_scf_info(f\"Nitrogen/run_{spacing}\")\n",
    "    table.append([spacing, total_energy, s_eigen, p_eigen])\n",
    "df = pd.DataFrame(table, columns=[\"Spacing\", \"Total_Energy\", \"S_eigen\", \"P_eigen\"])\n",
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "# shift values relativ to the last run (the one with the lowest spacing)\n",
    "df[df.columns[1:]] = df[df.columns[1:]] - df[df.columns[1:]].iloc[-1]\n",
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot total energy, S-eigen, P-eigen trend against Spacing\n",
    "df.plot(\n",
    "    x=\"Spacing\",\n",
    "    y=[\"Total_Energy\", \"S_eigen\", \"P_eigen\"],\n",
    "    style=\"o-\",\n",
    "    grid=True,\n",
    "    ylabel=\"Error (eV)\",\n",
    "    xlabel=\"Spacing (Å)\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "The results, for this particular example, are shown in the figure.\n",
    "In this figure we are actually plotting the error with respect to the most accurate results (smallest spacing).\n",
    "That means that we are plotting the '''difference''' between the values for a given spacing and the values for a spacing of 0.14 Å.\n",
    "So, in reading it, note that the most accurate results are at the left (smallest spacing).\n",
    "A rather good spacing for this nitrogen pseudopotential seems to be 0.18 Å.\n",
    "However, as we are usually not interested in total energies, but in energy differences, probably a larger one may also be used without compromising the results.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "## Methane molecule\n",
    "We will now move on to a slightly more complex system, the methane molecule CH<sub>4</sub>, and add a convergence study with respect to the box size.\n",
    "\n",
    "### Input\n",
    "As usual, the first thing to do is create an input file for this system. From our basic chemistry class we know that methane has a tetrahedral structure. The only other thing required to define the geometry is the bond length between the carbon and the hydrogen atoms. If we put the carbon atom at the origin, the hydrogen atoms have the coordinates given in the following input file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "inp_template_methane = Template(\n",
    "    \"\"\"\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Radius = $Radius_value*angstrom\n",
    "Spacing = $Spacing_value*angstrom\n",
    "\n",
    "CH = 1.2*angstrom\n",
    "%Coordinates\n",
    " \"C\" |           0 |          0 |           0\n",
    " \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    " \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    " \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\"\"\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "Here we define a variable CH that represents the bond length between the carbon and the hydrogen atoms, which simplifies the writing of the coordinates. We start with a bond length of 1.2 Å but that's not so important at the moment, since we can optimize it later. (You should not use 5 Å or so, but something slightly bigger than 1 Å is fine.)\n",
    "\n",
    "Some notes concerning the input file:\n",
    "* We do not tell **Octopus** explicitly which [BoxShape](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/boxshape) to use. The default is a union of spheres centered around each atom. This turns out to be the most economical choice in almost all cases.\n",
    "* We also do not specify the %[Species](https://www.octopus-code.org/documentation//13/variables/system/species/species) block. In this way, **Octopus** will use default pseudopotentials for both Carbon and Hydrogen. This should be OK in many cases, but, as a rule of thumb, you should do careful testing before using any pseudopotential for serious calculations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.makedirs(f\"3-methane_example/variable_spacing/run_0.22\")\n",
    "with open(f\"3-methane_example/variable_spacing/run_0.22/inp\", \"w\") as inp_file:\n",
    "    inp_file.write(\n",
    "        inp_template_methane.substitute(Spacing_value=0.22, Radius_value=3.5)\n",
    "    )\n",
    "\n",
    "subprocess.run(\n",
    "    \"octopus\", shell=True, cwd=f\"3-methane_example/variable_spacing/run_0.22\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!cat 3-methane_example/variable_spacing/run_0.22/static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "Now the question is whether these values are converged or not. This will depend on two things: the [BoxShape](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/boxshape), as seen above, and the [Species](https://www.octopus-code.org/documentation//13/variables/system/species/species). Just like for the Nitrogen atom, the only way to answer this question is to try other values for these variables.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 26 \"not fully\" 3-methane_example/variable_spacing/run_0.22/static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "###  Convergence with the spacing\n",
    "\n",
    "\n",
    "As before, we will keep all entries in the input file fixed except for the spacing that we will make smaller by 0.02 Å all the way down to 0.1 Å. So you have to run **Octopus** several times. We use a similar script as the one from the Nitrogen atom example:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "spacing_list = [0.20, 0.18, 0.16, 0.14, 0.12, 0.10]\n",
    "\n",
    "for spacing in spacing_list:\n",
    "    print(f\"Running octopus for spacing: {spacing}\")\n",
    "    os.makedirs(f\"3-methane_example/variable_spacing/run_{spacing}\")\n",
    "    with open(f\"3-methane_example/variable_spacing/run_{spacing}/inp\", \"w\") as inp_file:\n",
    "        inp_file.write(\n",
    "            inp_template_methane.substitute(Spacing_value=spacing, Radius_value=3.5)\n",
    "        )\n",
    "    subprocess.run(\n",
    "        \"octopus\", shell=True, cwd=f\"3-methane_example/variable_spacing/run_{spacing}\"\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24",
   "metadata": {},
   "outputs": [],
   "source": [
    "table_methane = []\n",
    "for spacing in spacing_list:\n",
    "    run_path = f\"3-methane_example/variable_spacing/run_{spacing}\"\n",
    "    total_energy, s_eigen, p_eigen = get_scf_info(run_path)\n",
    "    table_methane.append([spacing, total_energy, s_eigen, p_eigen])\n",
    "methane_df = pd.DataFrame(\n",
    "    table_methane, columns=[\"Spacing\", \"Total_Energy\", \"S_eigen\", \"P_eigen\"]\n",
    ")\n",
    "methane_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "methane_df.plot(\n",
    "    x=\"Spacing\",\n",
    "    y=[\"Total_Energy\"],\n",
    "    style=\"o-\",\n",
    "    grid=True,\n",
    "    ylabel=\"Total Energy (eV)\",\n",
    "    xlabel=\"Spacing (Å)\",\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "As you can see from this picture, the total energy is converged to within 0.1 eV for a spacing of 0.18 Å. So we will use this spacing for the next calculations.\n",
    "\n",
    "### Convergence with the radius\n",
    "\n",
    "\n",
    "Now we will see how the total energy changes with the [Radius](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/radius) of the box. We will change the radius in steps of 0.5 Å. You can change the input file by hand and run Octopus each time, or again use a small script:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "radius_list = [2.5, 3.0, 3.5, 4.0, 4.5, 5.0]\n",
    "\n",
    "for radius in radius_list:\n",
    "    print(f\"Running octopus for radius: {radius}\")\n",
    "    os.makedirs(f\"3-methane_example/variable_radius/run_{radius}\")\n",
    "    with open(f\"3-methane_example/variable_radius/run_{radius}/inp\", \"w\") as inp_file:\n",
    "        inp_file.write(\n",
    "            inp_template_methane.substitute(Spacing_value=0.18, Radius_value=radius)\n",
    "        )\n",
    "    subprocess.run(\n",
    "        \"octopus\", shell=True, cwd=f\"3-methane_example/variable_radius/run_{radius}\"\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "28",
   "metadata": {},
   "outputs": [],
   "source": [
    "table_methane_radius = []\n",
    "current_folder = os.getcwd()\n",
    "for radius in radius_list:\n",
    "    run_path = f\"3-methane_example/variable_radius/run_{radius}\"\n",
    "    total_energy, s_eigen, p_eigen = get_scf_info(run_path)\n",
    "    table_methane_radius.append([radius, total_energy, s_eigen, p_eigen])\n",
    "methane_radius_df = pd.DataFrame(\n",
    "    table_methane_radius, columns=[\"Radius\", \"Total_Energy\", \"S_eigen\", \"P_eigen\"]\n",
    ")\n",
    "methane_radius_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "methane_radius_df.plot(\n",
    "    x=\"Radius\",\n",
    "    y=[\"Total_Energy\"],\n",
    "    style=\"o-\",\n",
    "    grid=True,\n",
    "    ylabel=\"Total Energy (eV)\",\n",
    "    xlabel=\"Radius (Å)\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "If we again ask for a convergence up to 0.1 eV we should use a radius of 3.5 Å. How does this compare to the size of the molecule? Can you explain why the energy increases (becomes less negative) when one decreases the size of the box?\n",
    "\n",
    "\n",
    "[Go to *4-Visualisation.ipynb*](4-Visualisation.ipynb)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "7d86675be647ed983eca0751a5c5cd6e52cfa67869ea07edc2928c4d9b3ecdee"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
